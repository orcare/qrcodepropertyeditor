This Property Editor can be used on a ContentType to show a QR Code in the backend for the URL of the page being edited.  If a Content Node is associated with multiple hostnames then a QR code is shown for each hostname.

## How to use ##
To use the property editor you need to create a Data Type and select the QR Code Property Editor, then use the Data Type in your Content Type / Document Type.

## Next steps ##
The package uses the Google Charts API QR Code generator. This is deprecated, so in a future version I'll switch to ZXing.net.

Hat tip to our clients [Pulse Fitness](http://www.pulsemove.com/) for allowing us share this package.

If you need a QR Code for published content, then use Matt Brailsford's awesome [uQR Package](https://our.umbraco.org/projects/website-utilities/uqr/)