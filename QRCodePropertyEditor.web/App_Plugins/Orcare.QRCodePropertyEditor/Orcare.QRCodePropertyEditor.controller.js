﻿angular.module("umbraco")
    .controller("QRCodePropertyEditor.controller",
    function ($scope, $http, $location, editorState) {
        var urls = editorState.current.urls;
        var pageUrls = [];
		for (var i = 0; i < urls.length; i++) {
			var qualifiedUrl = { url:"", downloadlink: "qrcode_" + editorState.current.Id + ".png"};
			if (urls[i].substring(0,1) == "/") {
				qualifiedUrl.url = $location.protocol() + "://" + $location.host() + urls[i];
			} else {
				qualifiedUrl.url = urls[i];
			}
			pageUrls.push(qualifiedUrl);
		}
		$scope.pageUrls = pageUrls;
	}
);